
.. index::
   pair: 19 juin 2013; Antifa


.. _sipmrp_19_juin_2013:

========================
Nihiliste, moi ? Jamais
========================


La mort de Clément Méric nous a touchés.
Comme la majorité de la société française, certainement.

Chez nous comme dans la plupart des organisations qualifiées d’*extrême-gauche*
dans les médias et la classe politique traditionnelle, la mort n’éblouit
pas les yeux des militants.
Nous ne voulons pas de martyrs ni de fanatiques, mais des individus
conscients des besoins de la lutte collective, lucides sur le monde qui
les entoure et réfléchis dans leur engagement.

Nous n’avons pas de dogme intangible venu d’on ne sait quelle autorité
morale ou politique. Car nous avons appris à connaître, par notre longue
réflexion politique, sans cesse réactualisée, les écueils d’un engagement
et d’un credo qui ne pourraient être remis en question.

En cela, nous ne nous reconnaissons pas, pas plus que nous ne reconnaissons
de nombreuses organisations dont nous sommes proches, dans l’étiquette
*extrêmiste* si facilement collée sur nous.

La mort de Clément nous a sans doute touchés plus particulièrement.
Car Clément était des nôtres. Non seulement dans une sorte de grande
famille idéologique de gauche n’ayant pas renoncé à la lutte des classes,
mais aussi, plus simplement, parce que Clément, pendant quelque temps,
a été membre de la CNT et que beaucoup d’entre nous le côtoyaient dans
les manifestations, les réunions politiques et syndicales.
Ses choix et ses orientations lui appartiennent et s’il n’était plus
parmi nous depuis son arrivée à Paris, il restait notre camarade.

Certains d’entre nous ont donc, naturellement, été à son chevet dans les
premières heures de son admission à la Pitié-Salpêtrière.
Beaucoup d’entre nous, aux côtés de ses camarades de l’AFA, aux côtés
d’autres camarades, militants, syndicalistes, sans-grades et amoureux
du progrès social, se sont joints aux hommages et aux manifestations
suscités par son assassinat.

Mais la mort de Clément, malheureusement, ne nous a pas surpris, un peu
comme le malheur que l’on sent venir sans savoir où et quand il frappera,
ni sur qui il s’abattra. Depuis des mois et des années, le fascisme
reprend des couleurs.

Depuis les années 2000, les théories de l’extrême droite se sont banalisées.

Le 21 avril 2002 n’aura été qu’un symptôme d’un mal plus profond.
Par paresse, sans doute, parce que ces problématiques ne sont pas
plaisantes à soulever, parce que ceux qui étaient choqués ont eu
l’impression d’être seuls, sans doute, ou bien minoritaires, et que la
majorité a préféré regarder ailleurs, les reportages et les unes chocs,
les sondages douteux et les propos *borderline* se sont multipliés.

Voici donc revenu le temps des invasions barbares, des hordes d’immigrés,
qu’ils soient voleurs, terroristes ou parasites, par religion, par
essence ou par naissance...

Et voici en retour la célébration de la patrie, de la nation et de
l’identité, quand nos dirigeants se battent pour savoir si *nos héros*,
de Napoléon à Jeanne d’Arc, étaient de gauche ou de droite...
Puisque la République coloniale de Jules Ferry est devenue la référence,
gageons que l’année qui vient sera l’occasion de glorifier le 100e
anniversaire de cette belle guerre de 1914-1918, où le patriotisme
montrait son vrai visage.

Dans une offensive politico-médiatique généralisée contre les quartiers
populaires, les apprentis sorciers qui nous gouvernent ont entamé une
croisade contre la violence illégitime (celle des petits délinquants,
celle des mouvements sociaux, celle des ouvriers qui refusent d’être
envoyés à la casse), fermant les yeux sur celle des puissants et celle
qui les arrange.

Mais la vraie violence, elle, a monté au fur et à mesure que les
gouvernements de super-flics se sont succédés. Les violences contre les
personnes, de plus en plus gratuites, se sont multipliées, réactions
sans doute illégitimes, mais trop prévisibles du soubresaut de l’individu
tenant absolument à ne pas être le dernier en bas de l’échelle.
Le libéralisme économique, faisant sauter peu à peu les systèmes de la
solidarité des travailleurs, a mis les individus en concurrence, créant
pour les plus perdus un besoin vital : ne pas être le plus faible, trouver
un autre à mettre en dessous de soi, la femme, l’homosexuel, le chômeur
ou l’étranger.

Les blousons bombers marqués de drapeaux français ou d’insignes nazis
ont refait leur apparition en plein jour dans les grandes villes de
France. Au point que des militants fascistes ont eu suffisamment confiance
pour porter leurs couleurs ce jour-là dans le quartier Saint-Lazare et
se promener armés en centre-ville.

C’est contre cet ordre des choses que Clément, à son échelle, se battait.
Les grands partis, les éditorialistes des médias dominants, les représentants
de la classe dirigeante ont aujourd’hui beau jeu de verser des larmes de
crocodile. Ils ont laissé monter cette violence, faisant mine de ne pas
la comprendre. Et aujourd’hui, les militants antifascistes se voient
traînés par les mêmes dominants au niveau des fascistes qu’ils combattent.
*Violence illégitime*

Notre engagement, au sein de la CNT, comme au sein de nombreuses autres
de ces organisations que vous qualifiez d’extrême gauche, nous expose,
nous le savons, à cette stigmatisation.
Nous sommes, au mieux, les emmerdeurs qui se compliquent la vie.
Nous le voyons auprès de nos amis qui ne partagent pas nos opinions
(oui oui, c’est très possible. C’est ça, le recul et la réflexion sur
l’engagement évoqués plus haut...). Nous sommes des empêcheurs de
capitaliser en rond. Parce que nous refusons de la jouer perso, en
laissant la société aller dans le mur du moment que nous avons logement,
télé et voiture, parce que nous sommes pour une société d’entraide, que
nous refusons toutes les inégalités, que nous critiquons sans relâche
cette société de consommation et de concurrence à outrance, que nous
blasphémons en osant remettre en cause les divinités sacrées du *Marché*
et du *Capitalisme* (avec leurs capitales impérieuses), nous sommes
traités de marginaux.

Pourtant, à l'image de la société française, notre confédération, comme
beaucoup d’organisations que nous côtoyons dans les luttes, est composée
d’hommes et de femmes, de travailleurs, étudiants, salariés, chômeurs
ou retraités.

**Maintenant, nous pouvons vous dire ce qui a motivé ce texte.**

Un professeur de classe préparatoire s’est exprimé récemment dans les
colonnes de deux grands quotidiens nationaux, classés à gauche.
Comme nous tous, il pleurait la mort de Clément, un de ses anciens élèves.

Selon lui::

    notre jeune camarade n’était pas *nihiliste*, ce que
    *ses liens passés avec la CNT pourraient suggérer*.

Laissez-nous vous dire, Monsieur, que nous déplorons que la mort de notre
camarade soit prétexte à raconter n’importe quoi.

Les grands partis ne sont pas seuls à faire de la récupération.
Si un jour, Monsieur, votre curiosité vous poussait à venir à la rencontre
des militants de la CNT, vous seriez surpris, je pense, de l’incroyable
foi en l’humanité qui nous anime et nous aide à ne pas baisser les bras.
Vous seriez surpris de voir à quel point nous croyons possible des lendemains
meilleurs, dans le respect de chacun et l’intérêt de tous.
Et à quel point ceux que vous qualifiez de *nihilistes* ont à cœur la
lutte pour la dignité de l’être humain.

Nous nous permettons donc de vous poser ces dernières questions:
les *nihilistes* dont vous parlez ne seraient-ils pas ceux qui proclament
à travers télévisions, livres et journaux, qu’il n’y a rien d’autre à
faire qu’accepter l’inéluctable loi du marché, avec ses désastres humains,
sociaux et économiques;

les *nihilistes* ne sont-ils pas ceux qui prônent de laisser parader des
fascistes et leurs croix gammées dans les rues sans protester ?

les nihilistes ne sont-ils pas ceux qui estiment que seul un État fort
et sécuritaire pourra contrôler *la nature mauvaise de l’homme* ?

Et si vous persistez à nous croire extrémistes, Monsieur, je crois
qu’Albert Camus pourrait résumer notre credo absolu:
*Nous sommes condamnés à vivre ensemble ou à mourir ensemble.*


Le Syndicat Interprofessionnel de la Presse et des Médias-CNT
