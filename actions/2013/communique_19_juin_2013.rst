
.. index::
   pair: 19 juin 2013; Antifa


.. _sipm_19_juin_2013:

========================================================================================================
Antifasciste, antiraciste, oui... mais aussi anticapitaliste, autogestionnaire et internationaliste !
========================================================================================================




Antifasciste, antiraciste
==========================

Si nous ne lâcherons rien face à l’extrême droite, refusant qu’elle parade
dans la rue, agresse des militants, s’attaque à des locaux syndicaux,
violente des personnes pour leur seule *différence* (immigrés, homosexuels,
etc.) ou diffuse ses idées xénophobes, homophobes et autoritaires, nous
ne lâcherons rien non plus sur le terrain social !

Certes nous sommes antifascistes et antiracistes pour des questions de
valeurs et d’éthique.
Mais s’arrêter à cette position ne nous satisfait pas.

Car notre lutte contre l’extrême droite se fonde sur un projet de société
radicalement antagoniste.

Anticapitalistes !
==================

Parce que nous pensons qu’au cœur des drames sociaux, économiques,
écologiques et humains qui sévissent aujourd’hui, se trouve la division
de la société en classes sociales.
D’un côté ceux qui possèdent, dirigent et optimisent leurs profits ;
de l’autre ceux qui sont soumis au chantage à l’emploi, subissent la
dictature de la productivité, doivent obéir à leur hiérarchie et sont
privés de toute capacité de décision réelle.

Ce ne sont pas l’Union européenne, le Qatar, la Chine ou la *finance*
qu’il faut combattre, ni même le *grand capital et la mondialisation*.

Ce ne sont que des leurres, des paravents pour éviter une remise en cause
du capitalisme lui-même.

La lutte des classes existe, et nous devons la mener, et contester la
cause première des crises économiques et sociales : le fait qu’une minorité
possède les moyens de production et d’échange, décide de leur usage, de
nos besoins et de ce que nous devons accepter de faire pour satisfaire à
leur unique but, conserver, voire augmenter, leur profit !

La *financiarisation* ou à la *mondialisation* de l’économie ne sont
que les conséquences actuelles de cette logique.

Attaquons-nous donc au fond, le capitalisme lui-même, et non à ses formes.

Autogestionnaires !
===================

Parce que nous pensons que déléguer son pouvoir de décision à des leaders,
des partis politiques ou des experts-spécialistes, c’est déjà accepter
que d’autres gèrent nos vies sans nous demander notre avis et sans que le
moindre contrôle réel puisse exister.

Les projets sociaux-démocrates comme communistes autoritaires ont échoué
d’avoir sacralisé l’État, la représentation politique et les
*avant-garde éclairées*.

L’extrême droite ne sait que réclamer un renforcement de l’Etat, de la
hiérarchie et de l’autorité.

Plutôt que de continuer à chercher qui sera notre meilleur représentant,
le plus *compétent* ou le *moins pourri*, prenons nos affaires en main,
reprenons le contrôle de nos vies.

**La démocratie, oui, mais directe**.
**Des mandatés, oui, mais révocables.**
**Des syndicats, oui, mais autogérés !**

Un projet certes exigeant, mais qui est la seule alternative pour éviter
*trahisons*, *non-respect des promesses* et autres *affaires de corruption*
qui font le jeu de l’extrême droite.

Internationalistes !
=====================

Parce que nous pensons que les références à la *patrie*, au
*protectionnisme économique*, à *l’union nationale* ou au
*contrôle de l’immigration* ne sont que des reculs idéologiques pour
gagner des voix et flatter les bas instincts des électeurs influencés
par le discours volontairement simpliste de l’extrême droite.

Parler de patrie c’est faire croire que, parce que l’on est né français,
on a les mêmes intérêts que l’on soit patron ou salarié.

Parler de contrôle de l’immigration c’est nier les responsabilités
néocoloniales de l’Etat français et des entreprises françaises dans
l’appauvrissement volontaire et la destruction des systèmes sociaux des
pays dits *du Tiers-Monde* :

- pillage des ressources naturelles (pétrole, uranium, bois, cacao, etc.),
- mais aussi privatisations des services publics,
- imposition d’une agriculture intensive contre les cultures vivrière et
  d’autosubsistance,
- accords de soutien militaire et *policier* (rappelons-nous la Tunisie
  de Ben Ali, par ailleurs membre de l’internationale des partis socialistes...),
  etc.

Si Areva (entreprise bien française...) arrêtait de détruire des régions
entières du Mali ou du Niger pour récupérer l’uranium nécessaire à l’énergie
nucléaire française,
si Bouygues, Bolloré et consorts arrêtaient de soutenir les dictatures
des pays ouest-africains pour défendre leurs parts de marché, les habitants
de ces pays n’auraient sûrement pas la nécessité de venir chercher en
France de quoi survivre en abandonnant toute leur vie...

C’est aussi faire croire que le chômage viendrait des immigrés et non pas
des licenciements...
Or ce sont bien les directions des entreprises qui virent pour accroître
leurs profits, et l’État qui, sous prétexte d’économie, supprime des
milliers de postes de fonctionnaires et détruit les services publics.

Bref, parler de patrie ou de contrôle de l’immigration c’est encore une
fois accepter de céder à l’extrême droite qui ne cherche qu’à nier la
lutte des classes pour la remplacer par la lutte entre les peuples...

L’extrême droite se nourrit des contre-réformes des gouvernements
successifs face au capitalisme en crise ; elle se délecte des renoncements
idéologiques politiques et syndicaux ;
elle prospère sur le terrain du recours aux faux-semblants de patrie,
d’Etat protecteur et de personnalisation du pouvoir ;
elle grossit des affaires de corruption à répétition entre détenteurs
du pouvoir politique ou économique ;
elle s’extasie de la destruction des systèmes de solidarité comme du
repli sur soi et de la peur qui s’ensuivent.

Alors, si nous voulons réellement renvoyer l’extrême droite dans les
poubelles de l’histoire, nous devons reprendre l’offensive sociale,
renforcer l’auto-organisation des travailleurs, reconstruire des lieux
d’entraide et refuser de déléguer notre pouvoir de décision à qui que
ce soit.

L’extrême droite hait les syndicats, les grèves et les victoires sociales
parce qu’elle ne vit que de la misère, de la peur et de la victimisation.

Nous ne la vaincrons qu’en renversant enfin le rapport de force et en
conquérant enfin de nouveaux droits sociaux.

Ce sera l’un des enjeux fondamentaux de la lutte qui s’annonce contre
les nouvelles contre-réformes des retraites et de l’assurance chômage.

Céder une fois de plus sera la porte ouverte vers la résignation,
l’individualisme, la rancœur, la haine et le refuge vers *celui ou celle
qui gueule le plus fort*.

Il n’appartient qu’à nous de tout faire pour l’empêcher...

Face à l’extrême droite, Construisons un syndicalisme de classe,
autogestionnaire et émancipateur,

Pour un autre futur, sans exploitation ni domination !
