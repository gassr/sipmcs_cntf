.. index::
   ! meta-infos

.. _sipmcs_meta_infos:

=====================
Meta
=====================

Gitlab project
================

- https://gitlab.com/cnt-f/sipmcs


Fichiers sources
================

- https://gitlab.com/cnt-f/sipmcs/-/tree/main

Commits
----------

- https://gitlab.com/cnt-f/sipmcs/-/commits/main


Ticket (Issues en anglais): pour faire des remarques
-----------------------------------------------------

- https://gitlab.com/cnt-f/sipmcs/-/boards

Pipelines
-----------

- https://gitlab.com/cnt-f/sipmcs/-/pipelines


Membres
---------

- https://gitlab.com/cnt-f/sipmcs/-/project_members

pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:


.. _fichier_conf_py:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
