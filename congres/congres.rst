
.. _motions_sipm_rp:
.. _motions_sipmcs:

============================================
Congrès confédéraux SIPMCS
============================================

.. toctree::
   :maxdepth: 3

   2021/2021
   2014/2014
   2012/2012
   2010/2010
