
.. figure:: images/sipmcs.jpg
   :align: center

.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center

.. https://emoji.muan.co/
.. un·e  employé·e·s
.. pdfx <pdf> -t -o pdf-text.txt

.. _sipm_rp:
.. _sipmcs:
.. _sipmcs_cnt_f:

===============================================================================================================================================
SIPMCS **Syndicat Interprofessionnel des travailleuses et travailleurs de la Presse des Médias Culture Spectacle, Région Parisienne** CNT-F
===============================================================================================================================================

- https://twitter.com/SIPMCS_CNT
- http://www.cnt-f.org/sipm/

.. toctree::
   :maxdepth: 2

   actions/actions
   congres/congres

.. toctree::
   :maxdepth: 1
   :caption: Meta

   meta/meta
